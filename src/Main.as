/**
* An interactive aid to HIGH-INTENSITY CIRCUIT TRAINING
* outlined by Brett Klika & Chris Jordan
* http://journals.lww.com/acsm-healthfitness/Fulltext/2013/05000/HIGH_INTENSITY_CIRCUIT_TRAINING_USING_BODY_WEIGHT_.5.aspx
* 
* Excercising images © 2013 American College of Sports Medicine
* 'Boing' SFX by qubodup at freesound.org
* 
* Made by Fernando Ramallo http://www.byfernando.com
* 
* To the extent possible under law, Fernando Ramallo has waived all copyright and related or neighboring rights to HICT aid. This work is published from Argentina.
* http://creativecommons.org/publicdomain/zero/1.0/
* 
*/


package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.getTimer;
	
	
	public class Main extends Sprite 
	{
		
		/**
		 * Represents the amount of time in seconds that passed since last frame.
		 */
		static public var elapsed:Number;
		
		private var _total:Number = 0;
		private var _elapsed:Number = 0;
		
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			addEventListener(Event.ENTER_FRAME, update);
			
			addChild(new State());
		}
		
		private function update(e:Event):void 
		{
			//trace("update");
			var mark:uint = getTimer();
			
			var i:uint;
			//var soundPrefs:FlxSave;
			
			//Frame timing
			//Frame timing
			var ems:uint = mark-_total;
			_elapsed = ems/1000;
			//_console.mtrTotal.add(ems);
			_total = mark;
			Main.elapsed = _elapsed;
			if(Main.elapsed > 1000)
				Main.elapsed -= 1000;
			
			
		}
		
	}
	
}