package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.media.Sound;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author doisvnsd
	 */
	public class State extends Sprite 
	{
		[Embed(source = "../embed/01.jpg")] private var Img1:Class;
		[Embed(source = "../embed/02.jpg")] private var Img2:Class;
		[Embed(source = "../embed/03.jpg")] private var Img3:Class;
		[Embed(source = "../embed/04.jpg")] private var Img4:Class;
		[Embed(source = "../embed/05.jpg")] private var Img5:Class;
		[Embed(source = "../embed/06.jpg")] private var Img6:Class;
		[Embed(source = "../embed/07.jpg")] private var Img7:Class;
		[Embed(source = "../embed/08.jpg")] private var Img8:Class;
		[Embed(source = "../embed/09.jpg")] private var Img9:Class;
		[Embed(source = "../embed/10.jpg")] private var Img10:Class;
		[Embed(source = "../embed/11.jpg")] private var Img11:Class;
		[Embed(source = "../embed/12.jpg")] private var Img12:Class;
		[Embed(source = "../embed/boing.mp3")] private var Snd:Class;
		
		private var tf:TextField;
		
		private var stateTexts:Array = [
			"1. Jumping jacks",
			"2. Wall sit",
			"3. Push-up",
			"4. Abdominal crunch",
			"5. Step-up onto chair",
			"6. Squat",
			"7. Triceps dip on chair",
			"8. Plank",
			"9. High knees/running in place",
			"10. Lunge",
			"11. Push-up and rotation",
			"12. Side plank"
		];
		private var stateImgs:Array = [
			new Img1(),
			new Img2(),
			new Img3(),
			new Img4(),
			new Img5(),
			new Img6(),
			new Img7(),
			new Img8(),
			new Img9(),
			new Img10(),
			new Img11(),
			new Img12()
		];
		
		private var _counter:Number = 0;
		private var _timerGraphic:Sprite;
		private var _timerExerciseGraphic:Sprite;
		private var _transitionGraphic:Sprite;
		private var _graphicContainer:Sprite;
		private var _running:Boolean = false;
		private var _exercise:int = 0;
		private var _transition:Boolean = true;
		private var transitiontf:TextField;
		
		private var _sound:Sound;
		
		private const TIME_PER_EXERCISE:Number = 30; // secs
		private const TIME_PER_TRANSITION:Number = 10; // secs
		
		public function State() 
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		private function initialize(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			addEventListener(Event.ENTER_FRAME, update);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, start);
			
			_graphicContainer = new Sprite();
			addChild(_graphicContainer);
			
			_sound = new Snd() as Sound;
			
			
			tf = new TextField();
			addChild(tf);
			
			tf.x = 40;
			tf.y = 500;
			tf.width = 800;
			tf.defaultTextFormat = new TextFormat(null, 36);
			tf.filters = [new DropShadowFilter(0, 45, 0xffffff, 1, 4, 4, 50, BitmapFilterQuality.HIGH)];
			
			tf.text = "Press any key to start";
			
			transitiontf = new TextField();
			addChild(transitiontf);
			transitiontf.width = 800;
			transitiontf.y = 200;
			transitiontf.defaultTextFormat = new TextFormat(null, 64, 0xff0000, true,null,null,null,null,"center");
			transitiontf.text = "TRANSITION. WAIT.";
			transitiontf.visible = false;
			
			_timerGraphic = new Sprite();
			_timerGraphic.graphics.beginFill(0xff0000);
			_timerGraphic.graphics.drawRect(0, 0, 800, 20);
			_timerGraphic.graphics.endFill();
			_timerGraphic.scaleX = 0;
			addChild(_timerGraphic);
			
			
			_timerExerciseGraphic = new Sprite();
			_timerExerciseGraphic.graphics.beginFill(0x0000ff);
			_timerExerciseGraphic.graphics.drawRect(0, 0, 800, 20);
			_timerExerciseGraphic.graphics.endFill();
			_timerExerciseGraphic.scaleX = 0;
			_timerExerciseGraphic.y= 20;
		
			addChild(_timerExerciseGraphic);
			
			_transitionGraphic = new Sprite();
			_transitionGraphic.graphics.beginFill(0x9755ff);
			_transitionGraphic.graphics.drawRect(0, 0, 800, 600);
			_transitionGraphic.graphics.endFill();
		}
		
		private function update(e:Event):void
		{
			if (_running) {
				_counter += Main.elapsed;
				if (!_transition) {
					_timerGraphic.scaleX = _counter / TIME_PER_EXERCISE;
					_timerExerciseGraphic.scaleX = (_exercise / stateImgs.length) +  (_counter / TIME_PER_EXERCISE) * (1 / stateImgs.length);
				} else {
					_timerGraphic.scaleX = _counter / TIME_PER_TRANSITION;
				}
				
				
				if (!_transition && _counter >= TIME_PER_EXERCISE) {
					_counter -= TIME_PER_EXERCISE;
					_transition = true;
					_exercise++;
					if (_exercise >= stateImgs.length) {
						_exercise = -1;
						_running = false;
					}
					updateGraphic();
				} else if (_transition && _counter >= TIME_PER_TRANSITION) {
					_counter -= TIME_PER_TRANSITION;
					_transition = false;
					updateGraphic();
				}
			}
			
		}
		
		private function start(e:KeyboardEvent):void
		{
			_timerGraphic.scaleX = _timerExerciseGraphic.scaleX = 0;
			_counter = 0;
			_exercise = 0;
			_running = true;
			_transition = true;
			updateGraphic();
		}
		
		private function updateGraphic():void
		{
			if (_graphicContainer.numChildren > 0) _graphicContainer.removeChildAt(0);
			if (_exercise >= 0 && _exercise < stateImgs.length) {
				transitiontf.visible = _transition;
				if (!_transition) {
					
					_graphicContainer.addChild(stateImgs[_exercise]);
					_graphicContainer.getChildAt(0).alpha = 1;
					tf.text = stateTexts[_exercise];
				} else {
					_sound.play();
					_graphicContainer.addChild(stateImgs[_exercise]);
					_graphicContainer.getChildAt(0).alpha = 0.3;
					tf.text = "NEXT: " + stateTexts[_exercise];
				}
			} else {
				tf.text = "DONE YOU ARE GOOD HEALTH";
			}
			_sound.play();
		}
		
	}

}